# Histopathologic Cancer Detection

This repository presents an algorithm that identifies metastatic cancer on biomedical imaging. 
The implementation is based on the principle of deep learning : Setting up a Convolution Neuron Network (CNN).
This work focused on the virtual environment proposed by the Kaggle platform, which hosts the competition : 
"_Identify metastatic tissue in histopathologic scans of lymph node sections_".

## Project Score

This approach resulted in a public score of __0.9078__, corresponding to the area under the ROC curve between the expected probability 
and the observed target.